package com.kalyss.warngames.javascrapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

public class TurfooResultSenderTest {
    //  ping server DEV
    //  https://dev-warngames.kalyss.ch/warngame/ws/ping

    //  Récupérer les conditions générales selon la langue : [GET]
    //  https://dev-warngames.kalyss.ch/warngame/ws/datas/termsService?language={codeLanguage}
    //  https://dev-warngames.kalyss.ch/warngame/ws/datas/termsService?language=fr

    private static final String GET_URL = "https://dev-warngames.kalyss.ch/warngame/ws/admin/generateMessageKeyPMU";
    //  https://dev-warngames.kalyss.ch/warngame/ws/manual
    private String urlBase ="https://dev-warngames.kalyss.com/warngame/ws";

    private String GENERATED_KEY_CLAIREFONTAINE = "6-1-CLAIREFONTAINE-2019-07-01-FR";


    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testGetKey() {
        TurfooScrapper scr = new TurfooScrapper(false);

        Document localPage = TestUtil.loadLocalTestPage();
        List<PmuResultDTO> res = scr.parseDocument(localPage);

        Assert.assertNotNull(res);
        Assert.assertTrue(!res.isEmpty());

        // TODO parse json for
        TurfooResultSender sender = new TurfooResultSender(urlBase);

        Optional<TurfooResultSender.GeneratedKey> key = sender.getKey(res.get(0), 6, "/admin/generateMessageKeyPMU", "en");

        Assert.assertTrue(key.isPresent());

        Assert.assertTrue(key.get().toString().indexOf("CLAIREFONTAINE") > 0);
    }

    @Test
    public void testPingOld() {
        TurfooResultSender sender = new TurfooResultSender(urlBase);

        HttpConnection.Response resp = sender.pingOld(urlBase);

        Assert.assertNotNull(resp);

        String respString = resp.body();

        Assert.assertNotNull(respString);

        Assert.assertTrue(respString.indexOf("WARN Game Alive : ") >= 0);
    }

    @Test
    public void testPing() {
        TurfooResultSender sender = new TurfooResultSender(urlBase);

        Document resp = sender.ping(urlBase);

        Assert.assertNotNull(resp);

        String respString = resp.body().ownText();

        Assert.assertNotNull(respString);

        Assert.assertTrue(respString.indexOf("WARN Game Alive : ") >= 0);
    }

    @Test
    public void testGetOld() throws IOException {
        TurfooResultSender sender = new TurfooResultSender(urlBase);
        Map<String, String> params = new HashMap<>();

        params.put("date", "2019-07-01");
        params.put("number", "1");
        params.put("idChannel", "6");
        params.put("city", "CLAIREFONTAINE");
        params.put("language", "fr");

        HttpConnection.Response resp = sender.getOld(GET_URL, params);

        Assert.assertNotNull(resp);

        List ll = objectMapper.readValue(resp.body(), ArrayList.class);
        validateKey(ll);
    }

    @Test
    public void testGetNew() throws IOException {
        TurfooResultSender sender = new TurfooResultSender(urlBase);
        Map<String, String> params = new HashMap<>();

        params.put("date", "2019-07-01");
        params.put("number", "1");
        params.put("idChannel", "6");
        params.put("city", "CLAIREFONTAINE");
        params.put("language", "fr");

        Document doc = sender.get(GET_URL, params);

        Assert.assertNotNull(doc);

        List ll = objectMapper.readValue(doc.body().getElementsByTag("body").get(0).text(), ArrayList.class);
        validateKey(ll);
    }

    private void validateKey(List ll) {
        Assert.assertNotNull(ll);
        Assert.assertEquals(2, ll.size());
        Assert.assertEquals(GENERATED_KEY_CLAIREFONTAINE, ((Map<String, String>)ll.get(0)).get("generatedKey"));
    }
//    val url ="http://dev-warngames.kalyss.com/warngame/ws"
//
//    val realClient = new WarngamesClient(url)
//
//    val mockClient = spy(realClient)
//
//    //Common params
//    val calendar = Calendar.getInstance()
//  calendar.set(2015, 5, 4)
//
//    //result dto scrapped from the site
//    val result = new PmuResultDTO
//    result.courseLocation = "Genève"
//    result.courseNum = 1
//    result.courseHour = "11H00"
//    result.courseName = "Ma course"
//    result.resultDate = calendar.getTime
//    val formattedDate = new java.text.SimpleDateFormat("yyyy-MM-dd").format(result.resultDate)
//
//    //http params to get key
//    val paramsToGetkey = Seq(
//            "idChannel"->"6",
//            "number"->result.courseNum.toString,
//            "city"->result.courseLocation,
//            "date"->formattedDate)
//
//    //http params to post a new message
//    val paramsToInsertMessage = Seq(
//            "channel_list"->"6",
//            "title_message"->result.formattedTitle(),
//            "message_area"->result.formattedDetail(),
//            "channel_lang"-> "fr",
//            "generated_key"->"plop",
//            "source"->"scrapper")
//
//    //http params to update a message
//    val paramsToUpdateMessage = Seq(
//            "channel_list"->"6",
//            "title_message"->result.formattedTitle(),
//            "message_area"->result.formattedDetail(),
//            "channel_lang"-> "fr",
//            "generated_key"->"plop",
//            "source"->"scrapper",
//            "message_id"->"240")
//
//
//            "get request" should "raise a NetworkException when the url is wrong" in {
//        a [NetworkException] should be thrownBy realClient.ping("http://wrong.kalyss.com")
//
//    }
//
//  "post request" should "raise a NetworkException when the url is wrong" in {
//        a [NetworkException] should be thrownBy realClient.post("http://wrong.kalyss.com", Seq())
//    }
//
//    it should "post data for insertion" in {
//        reset(mockClient)
//        //mock return only the generated key -> insertion
//        val json = "[{\"generatedKey\": \"plop\"}]"
//
//
//        doReturn(new HttpResponse[String](json,200, Map())).when(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        doReturn(new HttpResponse[String]("plop",200,Map())).when(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToInsertMessage)
//
//        val key = mockClient.sendResult(result, 6, "scrapper", "/admin/generateMessageKeyPMU", "fr")
//
//        //check that post is call once to insert data
//        verify(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToInsertMessage)
//    }
//
//    it should "ignore the entry because the message is equals" in {
//        reset(mockClient)
//
//        //mocked generated key with same content than the scrapped result and no manual source
//        var json = "[{\"generatedKey\": \"plop\"},{\"businessDate\": \"2015-06-04 12:14:46\",\"details\": \"Genève - N°1 - Ma course\\n\\nArrivées provisoire :\",\"generatedKey\": \"plop\",\"idChannel\": 6,\"idMessage\": 240,\"source\": \"scrapper\",\"status\": 1,\"title\": \"Course du 04 juin 2015 11H00\",\"updateDate\": \"2015-06-04 13:52:07\"}]"
//        doReturn(new HttpResponse[String](json,200, Map())).when(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        doReturn(new HttpResponse[String]("plop",200,Map())).when(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToInsertMessage)
//
//        val key = mockClient.sendResult(result, 6, "scrapper", "/admin/generateMessageKeyPMU", "fr")
//
//        //we should never see post here post the data
//        verify(mockClient, times(0)).post(anyString(), anyObject())
//
//    }
//
//    it should "ignore the entry because the message has been manually modified even if the content differ" in {
//        reset(mockClient)
//
//        //mocked generated key with same content than the scrapped result and no manual source
//        var json = "[{\"generatedKey\": \"plop\"},{\"businessDate\": \"2015-06-04 12:14:46\",\"details\": \"Annecy - N°2 - Ma course\\n\\n\",\"generatedKey\": \"plop\",\"idChannel\": 6,\"idMessage\": 240,\"source\": \"manual\",\"status\": 1,\"title\": \"Course du 04 juin 2015\",\"updateDate\": \"2015-06-04 13:52:07\"}]"
//        doReturn(new HttpResponse[String](json,200, Map())).when(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        doReturn(new HttpResponse[String]("plop",200,Map())).when(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToInsertMessage)
//
//        val key = mockClient.sendResult(result, 6, "scrapper", "/admin/generateMessageKeyPMU", "fr")
//
//        //one call to getKey is made anyway
//        verify(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        //we should never see post here, data must not be sent
//        verify(mockClient, times(0)).post(anyString(), anyObject())
//    }
//
//    it should "post an update because the message received along with the generated key is different than the local one" in {
//        reset(mockClient)
//
//        //mocked generated key with same content than the scrapped result and no manual source
//        var json = "[{\"generatedKey\": \"plop\"},{\"businessDate\": \"2015-06-04 12:14:46\",\"details\": \"Annecy - N°2 - Ma course\\nArrivées provisoire :\\n\",\"generatedKey\": \"plop\",\"idChannel\": 6,\"idMessage\": 240,\"source\": \"scrapper\",\"status\": 1,\"title\": \"Course du 04 juin 2015\",\"updateDate\": \"2015-06-04 13:52:07\"}]"
//
//        doReturn(new HttpResponse[String](json,200, Map())).when(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        doReturn(new HttpResponse[String]("plop",200,Map())).when(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToUpdateMessage)
//
//        val key = mockClient.sendResult( result, 6, "scrapper", "/admin/generateMessageKeyPMU", "fr")
//
//        //we should see one post here to update message
//        verify(mockClient).get(url+"/admin/generateMessageKeyPMU", paramsToGetkey)
//        verify(mockClient).post(url+"/admin/insertOrUpdateMessage", paramsToUpdateMessage)
//    }

}
