package com.kalyss.warngames.javascrapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestUtil {

    private static final String PROPERTY_FILE = "config.properties";
    private static final String TURFOO_URL_PROPERTY = "testLocal.urlSource";

    public static Document loadLocalTestPage() {
        Properties props = loadProperties();
        String url = props.getProperty(TURFOO_URL_PROPERTY);
        InputStream in = null;
        if (url != null) {
            in = TestUtil.class.getClassLoader().getResourceAsStream(url);
        } else {
            throw new IllegalStateException(TURFOO_URL_PROPERTY + " not found in " + PROPERTY_FILE);
        }
        return parse(in);
    }

    public static Document parse(String url) {
        Document document = null;
        if (url != null) {
            try {
                document = Jsoup.connect(url)
                        .ignoreContentType(true)
                        .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                        .referrer("http://www.google.com")
                        .timeout(12000)
                        .followRedirects(true)
                        .get();
                return document;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new IllegalStateException(TURFOO_URL_PROPERTY + " unable to load");
        }
        return null;
    }

    public static Properties loadProperties() {
        Properties prop = new Properties();
        InputStream in = TestUtil.class.getClassLoader().getResourceAsStream(PROPERTY_FILE);

        try {
            prop.load(in);
        } catch (IOException e) {
            LoggerFactory.getLogger(TestUtil.class).error("Unable to load " + PROPERTY_FILE, e);
        }

        return prop;
    }

    public static Document parse(InputStream in) {
        Document document = null;
        if (in != null) {
            try {
                document = Jsoup.parse(in, "UTF-8", "https://turfoo.fr");
                return document;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            throw new IllegalStateException(TURFOO_URL_PROPERTY + " unable to load");
        }
        return null;
    }
}
