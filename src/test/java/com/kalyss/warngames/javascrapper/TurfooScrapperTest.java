package com.kalyss.warngames.javascrapper;

import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TurfooScrapperTest {

    private static final String TURFOO_REMOTE_URL = "http://www.turfoo.fr/programmes-courses/";

    @Test
    public void testParseLocal(){
        Document document = TestUtil.loadLocalTestPage();

        Assert.assertNotNull(document);

        TurfooScrapper scrapper = new TurfooScrapper(false);

        List<PmuResultDTO> results = scrapper.parseDocument(document);

        Assert.assertEquals(results.size(), 15);

        Assert.assertEquals(results.get(0).getCourseHour(), "13:50");
        Assert.assertEquals(results.get(0).getCourseLocation(), "CLAIREFONTAINE");
        Assert.assertEquals(results.get(0).getCourseResults().size(), 6);

        List<Integer> diff = (List<Integer>) Arrays.asList(new Integer[]{13,11,4,14,3,6});
        Assert.assertTrue(diff.equals(results.get(0).getCourseResults()));
        Assert.assertEquals(results.get(0).getCourseName(), "Prix Femmes Et Challenges Bby Cci Normandie (Prix Leopold D'");
        Assert.assertEquals(results.get(0).getCourseDetails(), "Steeple-chase");
        Calendar c = Calendar.getInstance();
        //c.set(2019, Calendar.JULY,1);
        c.setTime(results.get(0).getResultDate());

        Assert.assertTrue(c.get(Calendar.YEAR) == 2019 && c.get(Calendar.MONTH) == Calendar.JULY && c.get(Calendar.DAY_OF_MONTH) == 1);
    }

    @Test
    public void testParseRemote() {
        Document document = TestUtil.parse(TURFOO_REMOTE_URL);

        Assert.assertNotNull(document);

        TurfooScrapper scrapper = new TurfooScrapper(false);

        List<PmuResultDTO> results = scrapper.parseDocument(document);

        Assert.assertNotNull(results);
    }

    private Document loadRemoteTestPage() {
        InputStream in = null;
        try {
            URLConnection connection = new URL(TURFOO_REMOTE_URL).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "text/html");
            connection.setReadTimeout(10000);
            connection.setDoOutput(true);
            connection.connect();
            in = connection.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return TestUtil.parse(in);
    }

}
