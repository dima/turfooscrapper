package com.kalyss.warngames.javascrapper;

import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;

public class TurfooLauncher {

    //Error code return by the program
    public static final int SUCCES_CODE = 0;
    public static final int LAUNCHER_PARAM_ERROR_CODE = -1;
    public static final int CONFIG_ERROR_CODE = -2;
    public static final int NETWORK_ERROR_CODE = -3;
    public static final int PARSE_ERROR_CODE = -4;
    public static final int UNKNOWN_ERROR_CODE = -10;

    private static final String TURFOO_SITE_URL_KEY_SUFFIX = ".urlSource";
    private static final String TURFOO_CHANNEL_KEY_SUFFIX = ".idchannel";
    private static final String TURFOO_KEYSERVICE_KEY_SUFFIX = ".keyService";
    private static final String TURFOO_SERVICE_URL_KEY_SUFFIX = ".urlBaseService";

    private static final String TURFOO_PROPS_FILE = "config.properties";

    private static String PREFIX;

    public static void main(String[] args) {
        if (args != null && args.length == 1) {
            PREFIX = args[0];
        } else {
            PREFIX = "RealTurfooScrapper";
        }

        Properties config = loadProperties();

        TurfooResultSender sender = new TurfooResultSender(config.getProperty(PREFIX + TURFOO_SERVICE_URL_KEY_SUFFIX));

        TurfooScrapper scrapper = new TurfooScrapper(false);

        Document turfDoc = getDocument(config);
        List<PmuResultDTO> result = null;

        if (turfDoc != null) {
            result = scrapper.parseDocument(turfDoc);

            if (result.isEmpty()) {
                LoggerFactory.getLogger(TurfooLauncher.class).warn("No results found in parsed turfoo page, please check page structure or restart afternoon");
            } else {
                sender.sendResults(result, Integer.valueOf(config.getProperty(PREFIX + TURFOO_CHANNEL_KEY_SUFFIX)), config.getProperty(PREFIX + TURFOO_KEYSERVICE_KEY_SUFFIX));
            }
        } else {
            LoggerFactory.getLogger(TurfooLauncher.class).error("Impossible to connect to '" + config.getProperty(PREFIX + TURFOO_SITE_URL_KEY_SUFFIX) + "'");
        }

    }

    static Properties loadProperties() {
        Properties props = new Properties();

        try {
            InputStream in = TurfooLauncher.class.getClassLoader().getResourceAsStream(TURFOO_PROPS_FILE);
            props.load(in);
        } catch (IOException e) {
            LogManager.getLogManager().getLogger(TurfooLauncher.class.getName()).log(Level.SEVERE, "Unable to load 'config.properties' file");
        }
        return props;
    }

    private static Document getDocument(Properties config) {
        try {
            String url = config.getProperty(PREFIX + TURFOO_SITE_URL_KEY_SUFFIX);
            if (url.indexOf("http://") <= 0 && url.indexOf("https://") <= 0) {
                InputStream in = null;
                if (url != null) {
                    in = TurfooLauncher.class.getClassLoader().getResourceAsStream(url);
                } else {
                    throw new IllegalStateException(TURFOO_SITE_URL_KEY_SUFFIX + " not found in " + TURFOO_PROPS_FILE);
                }
                return Jsoup.parse(in, "UTF-8", "https://turfoo.fr");
            } else {

                return Jsoup.connect(url)
                        .ignoreContentType(true)
                        .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                        .referrer("http://www.google.com")
                        .timeout(12000)
                        .followRedirects(true)
                        .get();
            }
        } catch (IOException ex) {
            LoggerFactory.getLogger(TurfooLauncher.class).error("Error loading page : " + config.getProperty(PREFIX + TURFOO_SITE_URL_KEY_SUFFIX), ex);
            return null;
        }
    }

}
