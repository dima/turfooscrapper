package com.kalyss.warngames.javascrapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;
import com.kalyss.warngames.javascrapper.util.ScrapperTranslator;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.helper.HttpConnection.Response;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Récupérer un token généré par le serveur (unique id) pour un utilisateur avec ses éventuelles données qu'il envoie : [GET]

   PROD : https://warngames.kalyss.ch/warngame/ws/datas/randomKalyssToken?type={type}&model={model}&systemVersion={systemVersion}&customName={customName}&appVersion={appVersion}&language={codeLanguage}

   DEV : https://dev-warngames.kalyss.ch/warngame/ws/datas/randomKalyssToken?type={type}&model={model}&systemVersion={systemVersion}&customName={customName}&appVersion={appVersion}&language={codeLanguage}

   type : ios ou android ou unknown
 */

public class TurfooResultSender {

    private final String baseUrl;

    private final static String MANUAL_MESSAGE = "manual";
    private final static String INSERT_SERVICE = "/admin/insertOrUpdateMessage";

    private final static String INSERTED  = "inserted";
    private final static String UPDATED = "updated";
    private final static String IGNORED = "ignored";
    private final static String POST_ERROR = "error";

    private final static String CONTENT_TYPE = "Content-Type";
    private final static String CONTENT_TYPE_TEXT = "text/html;charset=utf-8";

    private final static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";//"Mozilla/5.0";;

    class GeneratedKey {
        final String generatedKey;
        final String idMessage;

        GeneratedKey(String generatedKey, String idMessage) {
            this.generatedKey = generatedKey;
            this.idMessage = idMessage;
        }

        public String toString() {
            if (idMessage == null) {
                return generatedKey;
            } else {
                return generatedKey + idMessage;
            }
        }

        public boolean isValid() {
            return this.generatedKey != null;
        }
    }

    public TurfooResultSender(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Response pingOld(String url) {
        return getOld(url+"/ping", Collections.emptyMap());
    }

    public Document ping(String url) {
        return get(url+"/ping", Collections.emptyMap());
    }

    public void sendResults(List<PmuResultDTO> results, Integer idChannel, String keyService) {
         LoggerFactory.getLogger(getClass()).info("Try to send " + results.size() + " results to " + baseUrl+INSERT_SERVICE);

         for (int i = 0; i < results.size(); i++) {
             sendResult(results.get(i) ,idChannel, keyService, "fr");
             sendResult(results.get(i) ,idChannel, keyService, "en");
         }
    }

    public String sendResult(PmuResultDTO result, Integer idChannel, String keyService, String lang) {
        String executionResult=null;

        //get key and if some call the insert service
        Optional<GeneratedKey> keyOp = getKey(result, idChannel, keyService, lang);
            // a key is provided, this message must be sent
            Map<String, String> params = null;
            if (keyOp.isPresent() && keyOp.get().isValid()) {
                params = new HashMap<>();
                params.put("channel_list", idChannel.toString());
                params.put("title_message", ScrapperTranslator.translateTitlePMU(result, lang));
                params.put("message_area", ScrapperTranslator.translateDetail(result, lang));
                params.put("channel_lang", lang);
                params.put("generated_key", keyOp.get().generatedKey);
                params.put("source", TurfooScrapper.class.getName());
                if (keyOp.get().idMessage != null) {
                    params.put("message_id", keyOp.get().idMessage);
                    LoggerFactory.getLogger(getClass().getName()).debug("updating message " + keyOp.get().idMessage + ", " + result.toLoggerString());
                    executionResult = UPDATED;
                } else {
                    LoggerFactory.getLogger(getClass().getName()).debug("Inserting message " + keyOp.get().idMessage + ", " + result.toLoggerString());
                    executionResult = INSERTED;
                }
                Document doc = post(baseUrl + INSERT_SERVICE, params);
                LoggerFactory.getLogger(getClass().getName()).debug("Inserting message " + keyOp.get().idMessage + ", " + result.toLoggerString());
                if (doc.title().indexOf("Unable to connect to ") >= 0) {
                    executionResult = POST_ERROR;
                }
            } else {
                executionResult = IGNORED;
            }
        return executionResult;
    }

    public Optional<GeneratedKey> getKey (PmuResultDTO result, Integer idChannel, String service, String lang) {

        Map<String, String> params = result.paramsForKeyRequest(lang);

        params.put("idChannel", idChannel.toString());

        Document doc = get(baseUrl+service, params);
        if (doc == null || doc.body() == null || doc.body().getElementsByTag("body").isEmpty()) {
            return Optional.empty();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        GeneratedKey newKey = null;
        try {
                List ll = objectMapper.readValue(doc.body().getElementsByTag("body").get(0).text(), ArrayList.class);
                Map<String, String> val = ll.isEmpty() ? null : (Map)ll.get(0);
                if (val != null && !val.isEmpty()) {
                    Map.Entry<String, String> entry = val.entrySet().iterator().next();
                    newKey = new GeneratedKey(entry.getKey(), entry.getValue());
                }
        } catch (IOException e) {
            LoggerFactory.getLogger(getClass()).error("Error new generated kalyss key reading ", e);
        }
        return newKey != null ? Optional.of(newKey) : Optional.empty();
    }

    public Document post(String url, Map<String, String> params) {
        try {
                String urlParameters = mapToParamString(params);
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );

                HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setAllowUserInteraction(true);
                connection.setInstanceFollowRedirects(true);

                connection.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_TEXT);
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty( "Accept-Encoding", "gzip");

                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("Content-Length", "" + Integer.toString(postData.length));
                connection.setRequestProperty("Content-Language", "en-US");
                //connection.setRequestProperty("Accept-Encoding", "");
                connection.setUseCaches (false);

                connection.setConnectTimeout(10000);
                connection.setReadTimeout(10000);


                connection.connect();

                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(postData);

                wr.flush();
                wr.close();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    LoggerFactory.getLogger(getClass()).error("Unable to connect to " + url + ", method : POST; http response : " + connection.getResponseCode() + "(" + connection.getResponseMessage() + ")");
                    Document doc = new Document(url);
                    Element e = doc.createElement("title");
                    e.text("Unable to connect to " + url + ", method : POST; http response : " + connection.getResponseCode() + "(" + connection.getResponseMessage() + ")");
                    doc.appendChild(e);
                    return doc;
                }
                InputStream in = connection.getInputStream();

                return Jsoup.parse(in, "UTF-8", "https://turfoo.fr");

        } catch (Exception e) {
            LoggerFactory.getLogger(getClass()).error("Unable to connect to " + url + ", method : POST", e);
        }
        return null;
    }

    public Document get(String url, Map<String, String> params) {
        try {
                String urlParameters = mapToParamString(params);
                HttpURLConnection connection = (HttpURLConnection)new URL(StringUtil.isBlank(urlParameters) ? url :  url + "?" + urlParameters).openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setAllowUserInteraction(true);
                connection.setInstanceFollowRedirects(true);
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty( "Accept-Encoding", "gzip");
                //connection.setRequestProperty("Content-Type", "text/html;charset=utf-8");
                connection.setConnectTimeout(10000);
                connection.setReadTimeout(10000);

                connection.connect();

                connection.getResponseCode();
                connection.getResponseMessage();
                InputStream in = connection.getInputStream();

                return Jsoup.parse(in, "UTF-8", "https://turfoo.fr");
        } catch (Exception e) {
            LoggerFactory.getLogger(getClass()).error("Unable to connect to " + url + ", method : GET", e);
        }
        return null;
    }

    /**
     * Use get (with standard java httpurl connection)
     * @param url
     * @param params
     * @return
     */
    @Deprecated
    // Unhandled content type. Must be text/*, application/xml, or application/xhtml+xml
    HttpConnection.Response getOld(String url, Map<String, String> params) {
        try {
                Connection connection = Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).method(Connection.Method.GET);
                connection.userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
                connection.timeout(10000);
                connection.data(params);
                Connection.Response resultResponse = connection.execute();

                return (HttpConnection.Response)resultResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String mapToParamString(Map<String, String> params) throws UnsupportedEncodingException{
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, String> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        return postData.toString();
    }
}
