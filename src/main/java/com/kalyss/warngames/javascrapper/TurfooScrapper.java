package com.kalyss.warngames.javascrapper;

import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;
import com.kalyss.warngames.javascrapper.util.ResultHelper;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.LogManager;

public class TurfooScrapper {

    private boolean pQuinte;

    private static final DateFormat HTML_DATE_FORMATTER = new java.text.SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);

    public TurfooScrapper(boolean pQuinte) {
        this.pQuinte = pQuinte;
    }

    /**
     * Parse the DOM to extract result. This where logic of each website must be implemented.
     * @param document
     * @return
     */
    public List<PmuResultDTO> parseDocument(Document document) {
        Date courseDate = extractDate(document.select("span[class=ttu turfoo-green]").first().text());
        Elements divCourses = document.select("div[class=pa2  horse-hiding bb b--black-20]");
        Elements spanTitles = document.select("span[class=db flex justify-around b]");
        List<PmuResultDTO> results = new ArrayList<>();
        for (int i = 0; i<divCourses.size(); i++) {
            results.addAll(extractResults(divCourses.get(i), extractLocation(spanTitles.get(i)), courseDate));
        }
        return results;
    }

    private List<PmuResultDTO> extractResults (Element div, String location, Date courseDate){

        //select each line
        Elements trs = div.select("div[class=flex flex-row justify-between items-center w-100 shadow-0 pa2 mb3 relative]");
        List<PmuResultDTO> ret = new ArrayList<>();
        for (int i = 0; i<trs.size(); i++) {
            Optional<PmuResultDTO> o = extractCourse(trs.get(i), location, courseDate);
            if (o.isPresent()) {
                ret.add(o.get());
            }
        }
        return ret;
    }

    private String extractLocation (Element div) {
        //Select location
        String locationRaw = div.text();
        int two = locationRaw.lastIndexOf(' ');
        int one = locationRaw.indexOf('•');
        return one > 0 && two > 0 ? locationRaw.substring(one+2, two-2).trim() : null;
    }

    private Optional<PmuResultDTO> extractCourse (Element row, String location, Date date) {
        if (pQuinte && isQuinteRow(row) || !pQuinte) {
            Elements resultCell = row.select("span[class=mid-gray f6]"); //  courseHour courseDetails courseResults
            if (resultCell != null && resultCell.first() != null) {
                String resultText = resultCell.first().ownText();
                if (ResultHelper.isResultValidNew(resultText)) {
                    PmuResultDTO result = new PmuResultDTO();
                    //result.courseNum=row.select("td[class=numcourse]").text.toInt
                    result.setCourseNum(Integer.parseInt(row.select("span[class=mr2 turfoo-green f4 flex justify-center b items-center]").text().substring(1)));
                    result.setCourseName(row.select("span[class=b f6 f5-ns myResearch flex items-center]").text());

                    List<String> resultData = ResultHelper.extractDotSeparatedList(resultText);
                    result.setCourseHour(resultData.get(0));
                    result.setCourseDetails(resultData.get(1));
                    result.getCourseResults().addAll(ResultHelper.extractDashSeparatedList(resultData.get(2)));

                    result.setCourseLocation(location);
                    //do not extract date form html, set current date by default
                    result.setResultDate(date);
                    return Optional.of(result);
                } else
                    return Optional.empty();
            } else {
                return Optional.empty();
            }
        }
        else
            return Optional.empty();
    }

    private Date extractDate (String dateRaw) {
        try {
            return HTML_DATE_FORMATTER.parse(dateRaw);
        } catch (ParseException e) {
            LogManager.getLogManager().getLogger(getClass().getName()).warning("Unparseable date : " + dateRaw + ", return NULL");
            return null;
        }
    }

    private Date extractDate (Element div) {
        //Select location
        String dateField = div.select("h1").text();
        return extractDate(dateField);
    }

    // <img class="pics_paris" src="/images/pics_paris/Quinte.png" loading="lazy" alt="Pari Quinté">
    private boolean isQuinteRow(Element row) {
        return row.select("img[class=pics_paris]").size() > 0;
    }

}
