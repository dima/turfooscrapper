package com.kalyss.warngames.javascrapper.dto;

import java.text.DateFormat;
import java.util.*;

public class PmuResultDTO {

    private static final DateFormat FRENCH_DATE_FORMAT = new java.text.SimpleDateFormat("dd MMMMM yyyy", new Locale("fr"));
    private static final DateFormat SHORT_FRENCH_DATE_FORMAT = new java.text.SimpleDateFormat("yyyyMMdd");
    private static final DateFormat KEY_REQUEST_DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd");

    private Integer courseNum = -1;

    private String courseLocation = "";

    private String courseName = "";

    private String courseDetails = "";

    private List<Integer> courseResults = new ArrayList<>();

    private List<Integer> nonPartant = new ArrayList<>();

    private String courseHour = "";

    private Date resultDate = null;

    public String courseKey(){
        String formattedDate = SHORT_FRENCH_DATE_FORMAT.format(resultDate);
        String locationLower = courseLocation.toLowerCase();
        return courseNum+locationLower+formattedDate;
    }

    public Integer getCourseNum() {
        return courseNum;
    }

    public void setCourseNum(Integer courseNum) {
        this.courseNum = courseNum;
    }

    public String getCourseLocation() {
        return courseLocation;
    }

    public void setCourseLocation(String courseLocation) {
        this.courseLocation = courseLocation;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseNam) {
        this.courseName = courseNam;
    }

    public String getCourseDetails() {
        return courseDetails;
    }

    public void setCourseDetails(String courseDetails) {
        this.courseDetails = courseDetails;
    }

    public List<Integer> getCourseResults() {
        return courseResults;
    }

    public List<Integer> getNonPartant() {
        return nonPartant;
    }

    public String getCourseHour() {
        return courseHour;
    }

    public void setCourseHour(String courseHour) {
        this.courseHour = courseHour;
    }

    public Date getResultDate() {
        return resultDate;
    }

    public void setResultDate(Date resultDate) {
        this.resultDate = resultDate;
    }

    public String toLoggerString() {
        return courseNum + " - " + courseName + " - " + courseLocation + " - " + FRENCH_DATE_FORMAT.format(resultDate);
    }

    public Map<String, String> paramsForKeyRequest(String lang) {
        Map<String, String> ret = new HashMap<>();
        ret.put("number", courseNum.toString());
        ret.put("city", courseLocation);
        ret.put("date", formatDateForKeyRequest());
        ret.put("language", lang);
        return ret;
    }

    public String formatDateForKeyRequest() {
        return KEY_REQUEST_DATE_FORMAT.format(resultDate);
    }
}
