package com.kalyss.warngames.javascrapper.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class ResultHelper {

    private static final String RESULT_VALIDATION_PATTERN = "^\\d+(\\s{0,1}-\\s{0,1}\\d+)+$";
    /**
     * Extract number in dash separated list
     * @param input the dash separated list
     * @return a List[Int]
     */
    public static List<Integer> extractDashSeparatedList(String input) {
        if(input!= null && input.length() > 0) {
            return Arrays.asList(input.split("-")).stream().map(String::trim).map(Integer::parseInt).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    /**
     * Extract strings in dot separated list
     * @param input the dash separated list
     * @return a List[String]
     */
    public static List<String> extractDotSeparatedList(String input) {
        if(input!= null && input.length()>0) {
            return Arrays.asList(input.split("•")).stream().map(String::trim).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    /**
     * Check if a result extracted form the website is actually a result with a list of number
     * @param result the result as string
     * @return
     */
    public static boolean isResultValidNew(String result) {
        List<String> tokens = extractDotSeparatedList(result);
        return tokens.size() == 3 && tokens.get(2).trim().matches(RESULT_VALIDATION_PATTERN);
    }
}
