package com.kalyss.warngames.javascrapper.util;

import com.kalyss.warngames.javascrapper.dto.PmuResultDTO;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class ScrapperTranslator {

    private static final DateFormat FRENCH_DATE_FORMAT = new java.text.SimpleDateFormat("dd MMMMM yyyy", new Locale("fr"));
    private static final DateFormat ENGLISH_DATE_FORMAT = new java.text.SimpleDateFormat("MMMM dd yyyy", new Locale("en"));

    private static Map<String, String> dictionnaryTitle = new HashMap<>();
    private static Map<String, String> dictionnaryDetail = new HashMap<>();

    static {
        dictionnaryTitle.put("Course du", "Race of");
        dictionnaryTitle.put("Tirage du", "Draw of");
        dictionnaryTitle.put("Midi", "Noon");
        dictionnaryTitle.put("Soir", "Evening");

        dictionnaryDetail.put("Arrivée:", "Result:");
        dictionnaryDetail.put("Arrivée provisoire:", "Provisional result:");
        dictionnaryDetail.put("Plat", "Flat");
        dictionnaryDetail.put("Course", "Race");
        dictionnaryDetail.put(" ans ", " years old ");
        dictionnaryDetail.put(" plus ", " more ");
        dictionnaryDetail.put(" Plus ", " More ");
        dictionnaryDetail.put("Attelé", "Harness");
        dictionnaryDetail.put("mètres", "meters");
        dictionnaryDetail.put("Départ à l'Autostart", "Mobile starting gate");
        dictionnaryDetail.put("Corde à droite", "Right handed");
        dictionnaryDetail.put("Corde à gauche", "Left handed");
        dictionnaryDetail.put("A Réclamer Course", "Selling Race");
        dictionnaryDetail.put("Handicap Course", "Handicap Race");
        dictionnaryDetail.put("Monté", "Mounted trotting");
        dictionnaryDetail.put("femelles", "female horses");
        dictionnaryDetail.put("mâles", "male horses");
        dictionnaryDetail.put("Males", "Male horses");
        dictionnaryDetail.put("Ligne droite", "Straightaway");
        dictionnaryDetail.put("Ligne Droite", "Straightaway");
        dictionnaryDetail.put("Groupe", "Group");
        dictionnaryDetail.put(" et ", " and ");
        dictionnaryDetail.put("Hongres", "Geldings");
        dictionnaryDetail.put("Course Européenne", "European Race");
        dictionnaryDetail.put("(PSF)", "(AF)");
        dictionnaryDetail.put("Apprentis et Jeunes Jockeys", "Apprentice and Young Jockeys");
        dictionnaryDetail.put("Haies", "Fences");
        dictionnaryDetail.put("partants", "runners");
        dictionnaryDetail.put("Course à réclamer", "Selling Race");
        dictionnaryDetail.put("A Réclamer", "Selling Race");
        dictionnaryDetail.put("(P.P.)", "(S.T.)");
        dictionnaryDetail.put("(G.P.)", "(L.T.)");
        dictionnaryDetail.put("Multiplicateur:", "Multiplier:");
        dictionnaryDetail.put("N°Chance:", "Lucky Number:");
        dictionnaryDetail.put("Anglo Arabes", "Anglo-Arabs");
        dictionnaryDetail.put("Arabes", "Arabs");
    }

    /**
     * "Course du "+ formattedDate+ " "+courseHour
     * @param dto
     * @param lang
     * @return
     */
    public static final String translateTitlePMU(PmuResultDTO dto, String lang){
        if (!Locale.FRENCH.toString().equals(lang)) {
            return dictionnaryTitle.get("Course du") + " " + ENGLISH_DATE_FORMAT.format(dto.getResultDate()) + " " + dto.getCourseHour();
        }
        return "Course du " + FRENCH_DATE_FORMAT.format(dto.getResultDate()) + " " + dto.getCourseHour();
    }

    // String toTranslate = "$courseLocation - N°$courseNum - $courseName\n$courseDetails\n$formattedResult";
    public static final String translateDetail(PmuResultDTO dto, String lang) {

        return dto.getCourseLocation() + " - N°" + dto.getCourseNum() + " - " + dto.getCourseName() + "\n" + dto.getCourseDetails() + "\n" + formateResult(dto, lang);
    }

    public static final String formateResult(PmuResultDTO dto, String lang) {
        String arrivee = dto.getCourseResults().size() < 5 ? "Arrivée provisoire:" : "Arrivée:";
        return dto.getCourseResults().isEmpty() ? "" : arrivee + "[" + dto.getCourseResults().stream()
                .map(item -> item.toString()) .collect(joining(" - ")) +"]";
    }
}
